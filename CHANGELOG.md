# Changelog
All notable changes to this repository will be documented in this file.

## [0.3] - 2019-09-27
### Added
- Edit _README.md_ file for more information

## [0.2] - 2019-09-27
### Added
- (Alpha) new version of Terms and Conditions (es1.1).
- added 1.0 stable version of Terms and Conditions

## [0.1] - 2019-08-01
### Added
- initial version of Terms and Conditions (es1.0).
- _.gitignore_ file with the exceptions required by this repository.