<img width="200" src="https://gitlab.com/kAoi97/brand/raw/master/Imagotype/Bitmap/kAoi97%20horisontal%20Imagotype.png" /><hr>

 # Terms and Conditions #
 
##About this repository

In this public access repository you can find an official backup copy of the Privacy Policy, Security Terms and Conditions of Use that regulate the access and use of the website hosted at the address https://www.kAoi97.net and their respective subdomains.

The use of that site attributes the condition of end user to your visitor, and expressly determines your full and unreserved acceptance of the Privacy Policy, Security Terms and Conditions of Use in this legal notice established.

## Changelog

If you have cloned this repository before, do not forget to check the [changelog](CHANGELOG.md) to make sure you are working with the _the latest applicable version_ to the repository.

## Contact

Unfortunately you still cannot access the FAQs or Q&A sections, it will be available soon.

If you have any concern, request complaint or claim regarding the terms and conditions, your rights or the application of these do not hesitate to contact me.

[www.kaoi97.net](https://www.kaoi97.net)  

[leonardo@kaoi97.net](mailto:leonardo@kaoi97.net)  

[(+57)314 4818811](https://t.me/kAoi97)  

### about copying the content hosted in this repository

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">kAoi97 WebMiner</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/leokaoi97" property="cc:attributionName" rel="cc:attributionURL">Leonardo Alvarado Guio</a>
is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />
Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/terms-and-conditions" rel="cc:morePermissions">https://www.kaoi97.net/terms-and-conditions</a>.


